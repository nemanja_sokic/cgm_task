package com.cgm.task.console.model;

import static com.cgm.task.console.util.Constants.MAX_STR_LEN;

public class Answer {
    private String answer;

    public Answer(String answer) {
        if(answer.length() > MAX_STR_LEN) throw new IllegalArgumentException("The answer has more then 255 chars.");
        this.answer = answer;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
