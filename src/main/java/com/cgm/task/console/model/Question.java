package com.cgm.task.console.model;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.cgm.task.console.util.Check.String.isBlank;
import static com.cgm.task.console.util.Check.String.isNotBlank;
import static com.cgm.task.console.util.Constants.MAX_STR_LEN;

public class Question {

    private String question;
    private List<Answer> answers;

    public Question(String aQuestion, List<Answer> aAnswers){
        if(aQuestion.length() > MAX_STR_LEN) throw new IllegalArgumentException("Question has more then 255 chars.");
        this.question = aQuestion;
        this.answers = aAnswers;
    }
    public Question(String aQuestion, String...aAnswers){
        if(aQuestion.length() > MAX_STR_LEN) throw new IllegalArgumentException("Question has more then 255 chars.");
        this.question = aQuestion;
        if(aAnswers != null){
            this.answers = Stream.of(aAnswers)
                    .filter(a -> isNotBlank(a))
                    .map(a -> new Answer(a))
                    .collect(Collectors.toList());
            if(this.answers.isEmpty()) throw new IllegalArgumentException("Question need to have at least 1 answer.");
        }else{
            throw new IllegalArgumentException("Question need to have at least 1 answer.");
        }
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    public boolean equalQuestion(String inputQuestion) {
        return this.question.equalsIgnoreCase(inputQuestion);
    }

    public static boolean validateInputQuestion(java.lang.String question) {
        int index = question.indexOf("?");
        if(index == -1) throw new IllegalArgumentException("Question does not have '?'");
        if(isBlank(question.substring(0,index))) throw new IllegalArgumentException("There is no question input.");
        Pattern p = Pattern.compile("\"([^\"]*)\"");
        Matcher m = p.matcher(question.substring(index, question.length()-1));
        if(!m.find()) {
            throw new IllegalArgumentException("Question needs to have at least one answer.");
        }
        return true;
    }
}
