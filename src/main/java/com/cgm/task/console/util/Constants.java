package com.cgm.task.console.util;

public class Constants {
    public static final int FIRST_OPTION = 1;
    public static final int SECOND_OPTION = 2;
    public static final int MAX_TRY = 3;
    public static final int MAX_STR_LEN = 255;

    public static final int ZERO_OPTION = 0;
    public static final String DEFAULT_ANSWER = "the answer to life, universe and everything is 42";
}
