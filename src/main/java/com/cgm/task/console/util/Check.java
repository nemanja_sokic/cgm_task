package com.cgm.task.console.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Check {
    public static class String {
        public static Integer validInteger(java.lang.String val) {
            boolean isValid = val != null && val.matches("^-?\\d+$");
            return isValid ? Integer.valueOf(val) : -1;
        }
        public static boolean isBlank(java.lang.String value) { return value == null || value.trim().equals(""); }
        public static boolean isNotBlank(java.lang.String s) {
            return !isBlank(s);
        }
    }
}
