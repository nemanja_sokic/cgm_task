package com.cgm.task.console;

import com.cgm.task.console.model.Answer;
import com.cgm.task.console.model.Question;

import java.util.Scanner;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.cgm.task.console.util.Check.String.*;
import static com.cgm.task.console.util.Constants.*;

public class Application {

    private static int exitCounter = 0;

    private static final Scanner input = new Scanner(System.in);
    public static List<Question> questionList = new ArrayList<>();

    public static void main(String[] args) {
        System.out.println("================ WELCOME TO CGM SERVICE ================");
        while (exitCounter < MAX_TRY) {
            try {
                prepareSystemQuestions();
                printOptions();
                String optionStr = input.nextLine();
                int option = validInteger(optionStr);
                if (option == FIRST_OPTION) {
                    askSpecificQuestion();
                } else if (option == SECOND_OPTION) {
                    addQuestion();
                } else if(option == ZERO_OPTION) {
                    return;
                } else {
                    printNotCorrect("Option does not exist or it is not valid.");
                }
            }catch (Exception e) {
                printNotCorrect(e.getMessage());
            }
        }
        System.out.println(String.format("Correcness is bad (%d/%d), exit the program. \n Buy, buy!",exitCounter,MAX_TRY));
        input.close();
        return;
    }

    private static void printOptions() {
        System.out.println("Choose an option:");
        System.out.println(FIRST_OPTION + ". Ask a specific question");
        System.out.println(SECOND_OPTION + ". Add questions and their answers");
        System.out.println("---------------------------------------------------------");
        System.out.println(ZERO_OPTION + " to exit program\n");
    }

    public static void prepareSystemQuestions() {
        questionList.addAll(Arrays.asList(
        new Question("How old are you?","10", "30", "50", "65"),
        new Question("What is your name?", "Peter","Alex","Edward"),
        new Question("What is your favorite food?", "Pizza", "Spaghetty", "Ice cream")
        ));
    }

    public static void addQuestion() {
        System.out.println("Please, add question in pattern '<question>? “<answer1>” “<answer2>” “<answerX>”'");
        String question = input.nextLine(); // Do you like running? "Yes" "No"
        if(Question.validateInputQuestion(question)){
            List<Answer> answers = getAnswerList(question);
            questionList.add(new Question(question.substring(0, question.indexOf("?")+1),answers));
            System.out.println("Question successfuly added.");
        }
    }

    public static List<Answer> getAnswerList(String question) {
        List<Answer> answers = new ArrayList<>();
        Pattern p = Pattern.compile("\"([^\"]*)\"");
        Matcher m = p.matcher(question);
        while(m.find()) {
            answers.add(new Answer(m.group(1)));
        }
        return answers;
    }

    public static void askSpecificQuestion() {
        System.out.println("Please, write some question to me. <Finish with '?'>");
        String inputQuestion = input.nextLine();
        if(isNotBlank(inputQuestion)){
            findQuestion(inputQuestion)
                    .getAnswers()
                    .forEach(a -> System.out.println(a.getAnswer()));
            System.out.println();
        }else{
            throw new IllegalArgumentException("There is no question.");
        }
    }

    public static Question findQuestion(String inputQuestion) {
        return questionList.stream()
                .filter(q -> q.equalQuestion(inputQuestion))
                .findFirst()
                .orElse(new Question("Default question?", DEFAULT_ANSWER));
    }

    private static void printNotCorrect(String message) {
        System.out.println(String.format("This is not correct inpt (%d/%d)", ++exitCounter, MAX_TRY));
        if(isNotBlank(message)){
            System.out.println(String.format("Message: %s", message));
        }
    }
}
