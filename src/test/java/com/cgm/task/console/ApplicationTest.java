package com.cgm.task.console;

import com.cgm.task.console.model.Answer;
import com.cgm.task.console.model.Question;
import com.cgm.task.console.util.Check;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.cgm.task.console.Application.*;
import static com.cgm.task.console.util.Constants.DEFAULT_ANSWER;
import static com.cgm.task.console.util.Constants.MAX_STR_LEN;
import static org.junit.jupiter.api.Assertions.*;

public class ApplicationTest {

    @BeforeAll
    public static void prepareSomeTestQuestion(){
        prepareSystemQuestions();
    }
    @Test
    public void createQuestion(){
        String inputQuestionLong = StringUtils.leftPad("1",MAX_STR_LEN + 1);
        String[] inputRegularAnswer = {"answer1","answer2"};

        assertThrows(IllegalArgumentException.class, () -> new Question(inputQuestionLong,inputRegularAnswer));

        String inputRegularQuestion = "How do you do?";
        String[] inputEmptyAnswer = {" "};

        assertThrows(IllegalArgumentException.class,() -> new Question(inputRegularQuestion, inputEmptyAnswer));

        String inputRegularQuestion2 = "How do you do?";
        String[] inputRegularAnswer2 = {"OK"};

        assertNotNull(new Question(inputRegularQuestion2,inputRegularAnswer2));

        String inputQuestion = "Is that OK?";
        String inputAnswerEmpty = " ";
        String inputAnswer = "Yes";

        assertThrows(IllegalArgumentException.class,() -> new Question(inputQuestion,inputAnswerEmpty));
        assertNotNull(new Question(inputQuestion,inputAnswer));
    }
    @Test
    public void createAnswer(){
        String inputAnswerLong = StringUtils.leftPad("1",MAX_STR_LEN + 1);
        String inputRegularAnswer = "answer";

        assertThrows(IllegalArgumentException.class, () -> new Answer(inputAnswerLong));
        assertNotNull(new Answer(inputRegularAnswer));
    }

    @Test
    public void askSpecificQuestionTest(){
        String inputExistedQuestion = "what is your name?";
        String inputNonExistedQuestion = "Is this question exists?";

        Question q = findQuestion(inputExistedQuestion);
        assertTrue(q.getQuestion().equalsIgnoreCase(inputExistedQuestion));

        Question q2 = findQuestion(inputNonExistedQuestion);
        assertTrue(q2.getAnswers().get(0).getAnswer().equals(DEFAULT_ANSWER));
    }

    @Test
    public void addQuestionTest(){
        final String questionInputNoSeparete = "Test question";
        assertThrows(IllegalArgumentException.class, () -> Question.validateInputQuestion(questionInputNoSeparete));
        final String questionInputNoQuestion = " ? \"answer1\"";
        assertThrows(IllegalArgumentException.class, () -> Question.validateInputQuestion(questionInputNoQuestion));
        final String questionInputNoAnswer = "This would be a question? no regular answer";
        assertThrows(IllegalArgumentException.class, () -> Question.validateInputQuestion(questionInputNoAnswer));
        final String questionInputAnswerEmpty = "This would be a question? \"\"";
        assertThrows(IllegalArgumentException.class, () -> Question.validateInputQuestion(questionInputAnswerEmpty));
        final String questionInputAnswerEmpty2 = "This would be a question? \" \"";
        assertThrows(IllegalArgumentException.class, () -> Question.validateInputQuestion(questionInputAnswerEmpty2));

        String regularQuestion = "Is this regular question? \"answer1\", \"answer2\"";
        List<Answer> answers = getAnswerList(regularQuestion);
        assertTrue(answers.size() == 2);
    }
}
