Application build command line: _gradle clean build_

Application run command line: _java -jar build/libs/cgm_task-0.1.0.jar_

Docker image run command:
- image: nemus123/cgm_jenkins:latest
- volume: jenkins_home -> /var/jenkins_home

Get image from repository:
_docker container run -p 8080:8080 -p 5000:5000 -d -v jenkins_home:/var/jenkins_home nemus123/cgm_jenkins:latest_
